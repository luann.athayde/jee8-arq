package com.domain.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

public class Service implements Serializable {

    @PersistenceContext(unitName = "jee8")
    protected EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }

}
