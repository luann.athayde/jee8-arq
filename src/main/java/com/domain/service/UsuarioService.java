package com.domain.service;

import com.domain.dto.UsuarioDTO;
import com.domain.entity.Usuario;
import com.domain.parser.UsuarioParser;

import javax.ejb.Stateless;

@Stateless
public class UsuarioService extends Service {

    public UsuarioDTO buscarPorId(Integer id) {

        Usuario usuario = getEntityManager().find(Usuario.class, id);
        if( usuario!=null ) {
            return UsuarioParser.get().paraDto(usuario);
        }

        return null;
    }

}
