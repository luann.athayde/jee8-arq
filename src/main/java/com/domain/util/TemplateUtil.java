package com.domain.util;

import java.io.InputStream;

/**
 *
 */
public final class TemplateUtil {

    public static final String TEMPLATES = "/templates/";
    public static final String EMPTY = "";

    public static String carregarTemplate(String nome) {
        try {
            InputStream in = TemplateUtil.class.getResourceAsStream(TEMPLATES + nome);
            byte[] template = new byte[in.available()];
            int read = in.read(template, 0, template.length);
            if (read == template.length) {
                return new String(template);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return EMPTY;
    }

}
