package com.domain.rest;

import com.domain.dto.UsuarioDTO;
import com.domain.service.UsuarioService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;

@Stateless
@Path("/usuario")
public class UsuarioRest implements Serializable {

    @EJB
    private UsuarioService usuarioService;

    @Path("buscar/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UsuarioDTO buscarPorId(@PathParam("id") Integer id) {
        if (id != null && id > 0) {
            UsuarioDTO dto = usuarioService.buscarPorId(id);
            // ---
            return dto;
        }
        return null;
    }

}
