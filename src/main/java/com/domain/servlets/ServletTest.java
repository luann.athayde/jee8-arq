package com.domain.servlets;

import com.domain.util.TemplateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/rss")
public class ServletTest extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            for (Cookie cookie : req.getCookies()) {
                cookie.setMaxAge(0);
                resp.addCookie(cookie);
            }

            String rssFeed = TemplateUtil.carregarTemplate("rss.xml");
            resp.setContentLengthLong(rssFeed.length());
            resp.getOutputStream().write(rssFeed.getBytes());
            resp.getOutputStream().close();
            resp.setStatus(200);
        }
        catch (Exception e) {
            resp.setStatus(500);
            // ---
            resp.sendError(501);
        }
    }

}
