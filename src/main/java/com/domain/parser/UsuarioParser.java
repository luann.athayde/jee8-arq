package com.domain.parser;

import com.domain.dto.UsuarioDTO;
import com.domain.entity.Usuario;

public class UsuarioParser extends Parser<Usuario, UsuarioDTO> {

    private static UsuarioParser instance = new UsuarioParser();
    public static UsuarioParser get() {
        return instance;
    }

    @Override
    public Usuario paraEntidade(UsuarioDTO dto) {

        Usuario domain = new Usuario();
        domain.setId(dto.getId());
        domain.setNome(dto.getNome());
        domain.setCpf(dto.getCpf());
        domain.setEmail(dto.getEmail());

        return domain;
    }

    @Override
    public UsuarioDTO paraDto(Usuario domain) {

        UsuarioDTO dto = new UsuarioDTO();
        dto.setId(domain.getId());
        dto.setNome(domain.getNome());
        dto.setCpf(domain.getCpf());
        dto.setEmail(domain.getEmail());

        return dto;
    }

}
