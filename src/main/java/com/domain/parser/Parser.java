package com.domain.parser;

public abstract class Parser<ENTITY, DTO> {

    public abstract ENTITY paraEntidade(DTO dto);
    public abstract DTO paraDto(ENTITY entity);

}
